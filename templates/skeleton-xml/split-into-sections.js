const fs = require('fs')

const data = require('./../../tmp/wlp-skeleton.json')

const sections = data
			.wlp_lex
			.map(([{entry_section}]) => entry_section.toLowerCase())                     // get section, e.g. 'a', 'ng', 'rd', ...
			.filter((value, index, self) => self.indexOf(value) === index) // retain unique values
			.map(function(section_file) {

				const section_data = { "wlp_lex" : data.wlp_lex.filter(([{entry_section}]) => entry_section.toLowerCase() == section_file) }

				fs.writeFileSync('tmp/wlp-section-' + section_file + '.json', JSON.stringify(section_data, null, 2))

				console.log('Section written: ' + section_file)

			})
